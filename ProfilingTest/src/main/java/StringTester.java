import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

public class StringTester {
    public void test() {
        int num = 1_000_000;

        final Stopwatch stopWatch = Stopwatch.createUnstarted();
        long elapsed = 0;
        int loops = 5;

        for (int i = 0; i < loops; i++) {
            stopWatch.reset().start();
            testBuilder(num);

            elapsed = stopWatch.elapsed(TimeUnit.MILLISECONDS);
            System.out.println("Builder - Time elapsed (ms): " + elapsed);
        }

        System.out.println("----------------------------------------");

        for (int i = 0; i < loops; i++) {
            stopWatch.reset().start();

            testConcat(num);

            elapsed = stopWatch.elapsed(TimeUnit.MILLISECONDS);
            System.out.println("Concat  - Time elapsed (ms): " + elapsed);
        }

        System.out.println("----------------------------------------");
    }

    private void testConcat(final int num) {
        int max = num / 10;
        for (int i = 0; i < max; i++) {
            String str = "";
            for (int j = 0; j < 10; j++) {
                str += "b";
            }
        }
    }

    private void testBuilder(int num) {
        StringBuilder sb = new StringBuilder();
        int max = num / 10;

        for (int i = 0; i < 10; i++) {
            sb.setLength(0);
            sb.setLength(max);
            // sb = new StringBuilder();
            for (int j = 0; j < max; j++) {
                sb.append("a");
            }
            // final String str = sb.toString();
        }
    }
}

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;

import com.google.common.base.Stopwatch;
import com.google.common.base.Strings;

/**
 * This class is meant to test some optimization strategies:
 * 
 * - string builder vs concat   : HUGE   - If strings are being built in ways that the compiler can't optimize (loops)
 * - parallel streams           : HUGE   - (numCores)x if the streams don't share resources. If many shared resources, performance can actually degrade significantly
 * - caching reused objects     : HUGE   - if object creation is expensive, more expensive = more savings. For simple objects with only a few basic fields this actually degrades performance.
 * - collections size init      : MEDIUM - final size of list matters, bigger list = more savings
 * - containskey vs nullcheck   : SMALL  - depends on complexity of equals() and hashcode() - more complex = more savings
 * - array/prim vs list/wrapper : 
 * - garbage collection time    : amount of memory available matters greatly...
 * -                              setting the memory as high as it can go will help with this...
 * -                              new'ing things contributes to gc as well
 */
public class Main {
    private static final TimeUnit      timeUnit      = TimeUnit.MILLISECONDS;
    private static final DecimalFormat df            = new DecimalFormat("##0.00");
    private static final int           outerLoopSize = 1_000_000;
    private static final int           innerLoopSize = 1000;

    public static void main(String[] args) throws InterruptedException {
        new Main().run();
    }

    private Map<String, Long> resultsMap = new HashMap<>();

    public void run() throws InterruptedException {
        final Stopwatch stopWatch = Stopwatch.createStarted();

        final boolean notParallel = false;
        final boolean parallel = true;
        final boolean notNullcheck = false;
        final boolean nullcheck = true;
        final boolean cached = true;
        final boolean notCached = false;
        final boolean strBuilder = true;
        final boolean notStrBuilder = false;
        final boolean initList = true;
        final boolean notInitList = false;

        final boolean DONT_CARE = false;

        final boolean skipSlow = true;

        // @formatter:off
        if (!skipSlow)
        {
        runTest(notStrBuilder, notParallel, notCached, notInitList, notNullcheck);
        runTest(notStrBuilder, notParallel, notCached,    initList, notNullcheck);
        runTest(notStrBuilder, notParallel, notCached, notInitList,    nullcheck);
        runTest(notStrBuilder, notParallel, notCached,    initList,    nullcheck);
        runTest(notStrBuilder, notParallel,    cached, notInitList, notNullcheck);
        runTest(notStrBuilder, notParallel,    cached,    initList, notNullcheck);
        runTest(notStrBuilder, notParallel,    cached, notInitList,    nullcheck);
        runTest(notStrBuilder, notParallel,    cached,    initList,    nullcheck);
        runTest(notStrBuilder,    parallel, notCached, notInitList, notNullcheck);
        runTest(notStrBuilder,    parallel, notCached,    initList, notNullcheck);
        runTest(notStrBuilder,    parallel, notCached, notInitList,    nullcheck);
        runTest(notStrBuilder,    parallel, notCached,    initList,    nullcheck);
        runTest(notStrBuilder,    parallel,    cached, notInitList, notNullcheck);
        runTest(notStrBuilder,    parallel,    cached,    initList, notNullcheck);
        runTest(notStrBuilder,    parallel,    cached, notInitList,    nullcheck);
        runTest(notStrBuilder,    parallel,    cached,    initList,    nullcheck);
        runTest(   strBuilder, notParallel, notCached, notInitList, notNullcheck);
        runTest(   strBuilder, notParallel, notCached,    initList, notNullcheck);
        runTest(   strBuilder, notParallel, notCached, notInitList,    nullcheck);
        runTest(   strBuilder, notParallel, notCached,    initList,    nullcheck);
        runTest(   strBuilder, notParallel,    cached, notInitList, notNullcheck);
        runTest(   strBuilder, notParallel,    cached,    initList, notNullcheck);
        runTest(   strBuilder, notParallel,    cached, notInitList,    nullcheck);
        runTest(   strBuilder, notParallel,    cached,    initList,    nullcheck);
        }                                   
                                                                  
        runTest(   strBuilder,    parallel, notCached, notInitList, notNullcheck);
        runTest(   strBuilder,    parallel, notCached,    initList, notNullcheck);
        runTest(   strBuilder,    parallel, notCached, notInitList,    nullcheck);
        runTest(   strBuilder,    parallel, notCached,    initList,    nullcheck);
        runTest(   strBuilder,    parallel,    cached, notInitList, notNullcheck);
        runTest(   strBuilder,    parallel,    cached,    initList, notNullcheck);
        runTest(   strBuilder,    parallel,    cached, notInitList,    nullcheck);
        runTest(   strBuilder,    parallel,    cached,    initList,    nullcheck);
        // @formatter:on

        System.out.println();
        System.out.println("==================================");
        System.out.println();

        resultsMap.entrySet()
                  .stream()
                  .sorted(Map.Entry.<String, Long>comparingByValue())
                  .forEach(eachResult -> System.out.println(String.format("%6d",
                                                                          eachResult.getValue())
                          + "(ms)" + " -> " + eachResult.getKey()));

        System.out.println();

        final long elapsed = stopWatch.stop()
                                      .elapsed(timeUnit);
        final String elapsedStr = String.format("%6d",
                                                elapsed)
                + "(ms)";

        System.out.println("DONE - " + elapsedStr);

    }

    private void runTest(final boolean strBuilder,
                         final boolean parallel,
                         final boolean cached,
                         final boolean initList,
                         final boolean nullcheck) {
        // @formatter:off
        final String cachedOrNah      = cached     ? "cached"     : "new'd "    ;
        final String parallelOrNah    = parallel   ? "parallel"   : "linear  "  ;
        final String nullCheckOrNah   = nullcheck  ? "nullcheck"  : "contains " ;
        final String strBuilderOrNah  = strBuilder ? "strBuilder" : "concat    ";
        final String initListOrNah    = initList   ? "initList"   : "autoList"  ;
        // @formatter:on

        final String testKey = strBuilderOrNah + ", " + parallelOrNah + ", " + cachedOrNah + ", " + initListOrNah + ", " + nullCheckOrNah;

        final List<Long> results = new ArrayList<>(15);

        final String testName = " :: " + testKey;
        System.out.println(testName);
        System.out.println("\telapsed (ms) \tgcTime (ms) \tgcTime (%)");

        final Map<Integer, MyClass> cache;
        if (initList) {
            final int initialCacheSize = (int) (innerLoopSize * 1.5);
            cache = parallel ? new ConcurrentHashMap<>(initialCacheSize) : new HashMap<>(initialCacheSize);
        } else {
            cache = parallel ? new ConcurrentHashMap<>() : new HashMap<>();
        }

        // preload cache with known values to use so we never have to new anything
        for (int each2 = 0; each2 < innerLoopSize; each2++) {
            MyClass myClass = new MyClass(each2);
            cache.put(each2,
                      myClass);
        }

        for (int eachTestRun = 0; eachTestRun < 10; eachTestRun++) {
            cache.clear();
            long gcTimeStart = getGarbageCollectionTime();
            final Stopwatch stopWatch = Stopwatch.createStarted();

            final IntConsumer doIt = each -> {
                final List<MyClass> list;

                if (initList) {
                    final int initialListSize = innerLoopSize + 10;
                    list = new ArrayList<>(initialListSize);
                } else {
                    list = new ArrayList<>();
                }

                String str = "";
                final StringBuilder sb = new StringBuilder();

                for (int each2 = 0; each2 < innerLoopSize; each2++) {
                    MyClass myClass;

                    if (cached) {
                        if (nullcheck) {
                            // This turns out to be a lot faster since we are not
                            // performing two lookups, if the object creation is expensive
                            myClass = cache.get(each2);
                            if (myClass == null) {
                                myClass = new MyClass(each2);
                                cache.put(each2,
                                          myClass);
                            }
                        } else {
                            if (!cache.containsKey(each2)) {
                                myClass = new MyClass(each2);
                                cache.put(each2,
                                          myClass);
                            } else {
                                myClass = cache.get(each2);
                            }
                        }
                    } else {
                        myClass = new MyClass(each2);
                    }

                    if (strBuilder) {
                        sb.append("-");
                    } else {
                        str += "x";
                    }

                    list.add(myClass);
                }
            };

            if (parallel) {
                IntStream.range(0,
                                outerLoopSize)
                         .parallel()
                         .forEach(doIt);
            } else {
                IntStream.range(0,
                                outerLoopSize)
                         .forEach(doIt);
            }

            // @formatter:off
            final long   elapsed          = stopWatch.stop() .elapsed(timeUnit);
            final long   gcTimeStop       = getGarbageCollectionTime();
            final long   gcTimeTotal      = (gcTimeStop - gcTimeStart);
            final double gcTimePercentage = 100 * (double) gcTimeTotal / elapsed;
            
            final String elapsedStr       = String.format("%6d", elapsed);
            final String gcTimeStr        = String.format("%6d", gcTimeTotal);
            final String gcTimePercentStr = "[" + Strings.padStart(df.format(gcTimePercentage), 6, ' ') + "%]";
            // @formatter:on            

            final String result = "\t" + elapsedStr + "\t\t" + gcTimeStr + "\t\t" + gcTimePercentStr;

            System.out.println(result);
            results.add(elapsed);
        }

        // Enabling this will take the best case
        // filterData(results);

        // Use the mean average
        final Long avg = average(results);

        resultsMap.put(testKey,
                       avg);
    }

    private void filterData(List<Long> list) {
        // boolean keepLooking = true;

        Collections.sort(list);

        final Long value = list.get(0);
        list.clear();
        list.add(value);

        // while (keepLooking) {
        // keepLooking = false;
        // final Long avg = average(list);
        // //System.out.println("average: " + avg);
        //
        // final int last = list.size() - 1;
        // Long eachLong = list.get(last);
        // long distance = Math.abs(eachLong - avg);
        // float percent = 1.0f * distance / avg;
        //
        // //System.out.print(" eachlong: " + eachLong + ", Distance: " + distance + " (" + percent + ")");
        // if (percent > 0.25) {
        // //System.out.print(" removed " + eachLong);
        // list.remove(last);
        // keepLooking = true;
        // }
        // //System.out.println();
        // }
    }

    private Long average(List<Long> list) {
        final OptionalDouble avgDouble = list.stream()
                                             .mapToLong(Long::longValue)
                                             .average();

        final Long avg = Double.valueOf(avgDouble.getAsDouble())
                               .longValue();
        return avg;
    }

    private static long getGarbageCollectionTime() {
        long collectionTime = 0;
        for (GarbageCollectorMXBean garbageCollectorMXBean : ManagementFactory.getGarbageCollectorMXBeans()) {
            collectionTime += garbageCollectorMXBean.getCollectionTime();
        }
        return collectionTime;

    }

    private class MyClass {
        public final int id;

        public MyClass(int id) {
            super();
            this.id = id;
        }
    }
}

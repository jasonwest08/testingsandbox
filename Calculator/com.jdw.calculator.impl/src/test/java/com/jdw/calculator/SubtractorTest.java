//package com.jdw.calculator;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//
//public class SubtractorTest
//{
//	private static final String DP_SUBTRACT_DATA = "subtractData";
//
//	@DataProvider(name = DP_SUBTRACT_DATA)
//	public Object[][] createSubtractData()
//	{
//		return new Object[][] { { 0, 0, 0 }, { 1, 1, 0 }, {1, 2, -1 }};
//	}
//
//	@Test(dataProvider = DP_SUBTRACT_DATA)
//    	public void testSubtract(int a, int b, int expectedValue)
//    	{
//    		Subtractor subtractor = new Subtractor();
//    		int result = subtractor.subtract(a, b);
//    		assertThat(result).isEqualTo(expectedValue);
//    	}
//}

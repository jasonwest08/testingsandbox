//package com.jdw.calculator;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//
//public class MultiplierTest
//{
//    @DataProvider(name = "dp_Multiply")
//    public static Object[][] dp_Multiply()
//    {
//        // @formatter:off
//		return new Object[][] { {  0,  0,  0 }, 
//								{  1,  0,  0 }, 
//								{  1,  2,  2 }, 
//								{ -2,  2, -4 }, 
//								{  2,  4,  8 } };
//
//        // @formatter:on
//    }
//
//    @Test(dataProvider = "dp_Multiply")
//    public void testMultiply_givenValidValues_returnExpectedSum(int x,
//                                                                int y,
//                                                                int expectedSum)
//    {
//        Multiplier multiplier = new Multiplier();
//        int product = multiplier.multiply(x,
//                                          y);
//        assertThat(product).isEqualTo(expectedSum);
//    }
//}

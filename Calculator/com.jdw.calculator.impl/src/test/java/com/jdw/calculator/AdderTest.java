//package com.jdw.calculator;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//
//public class AdderTest
//{
//    @DataProvider(name = "dp_Add")
//    public static Object[][] dp_Add()
//    {
//        // @formatter:off
//		return new Object[][] { { new int[] {           },  0 }, 
//								{ new int[] {  1        },  1 } , 
//								{ new int[] {  1,  2    },  3 } , 
//								{ new int[] {  1,  2, 3 },  6 } , 
//								{ new int[] { -1, -2    }, -3 } };
//
//        // @formatter:on
//    }
//
//    @Test(dataProvider = "dp_Add")
//    public void testAdd_givenMultipleValues_returnExpectedSum(int[] values,
//                                                              int expectedSum)
//    {
//        Adder adder = new Adder();
//        int sum = adder.add(values);
//        assertThat(sum).isEqualTo(expectedSum);
//    }
//}

//package com.jdw.calculator;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.mockito.ArgumentMatchers.anyInt;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//import org.assertj.core.api.SoftAssertions;
//import org.mockito.ArgumentCaptor;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//
//public class CalculatorTest
//{
//    
//    private Calculator calculator;
//
//    private IAdder mockAdder;
//    //private ISubtractor mockSubtractor;
//    //private IMultiplier mockMultiplier;
//    
//    @BeforeMethod
//    public void setup()
//    {
//        mockAdder = mock(IAdder.class);
//        //mockSubtractor = mock(ISubtractor.class);
//        //mockMultiplier = mock(IMultiplier.class);
//
//        calculator = new Calculator(mockAdder);//,
//                                    //mockSubtractor,
//                                    //mockMultiplier);
//    }
//
//    @Test
//    public void testAdd()
//    {
//        when(mockAdder.add(1,
//                           2)).thenReturn(3);
//        when(mockAdder.add(2,
//                           2)).thenReturn(4);
//
//        assertThat(calculator.add(1,
//                                  2)).isEqualTo(3);
//        assertThat(calculator.add(2,
//                                  2)).isEqualTo(4);
//    }
//
////    @Test
////    public void testSubtract()
////    {
////        // Run the foo method with the mock
////        calculator.subtract(2,1);
////
////        // Capture the argument of the subtract function
////        ArgumentCaptor<Integer> captor1 = ArgumentCaptor.forClass(Integer.class);
////        ArgumentCaptor<Integer> captor2 = ArgumentCaptor.forClass(Integer.class);
////        verify(mockSubtractor, times(1)).subtract(captor1.capture(), captor2.capture());
////
////        // Assert the argument
////        int actual1 = captor1.getValue();
////        int actual2 = captor2.getValue();
////
////        // use SoftAssertions instead of direct assertThat methods
////        SoftAssertions softly = new SoftAssertions();
////        
////        softly.assertThat(actual1).as("First argument").isEqualTo(2);
////        softly.assertThat(actual2).as("Second argument").isEqualTo(1);
////
////        // Don't forget to call SoftAssertions global verification !
////        softly.assertAll();
////    }
////
////    @Test
////    public void testMultiply()
////    {
////        calculator.multiply(1,
////                            2);
////
////        verify(mockMultiplier,
////               times(1)).multiply(1,
////                                  2);
////        verify(mockMultiplier,
////               times(1)).multiply(anyInt(),
////                                  anyInt());
////    }
//}

package com.jdw.calculator;

import com.jdw.calculator.api.ISubtractor;

import dagger.Module;
import dagger.Provides;

@Module
public class SubtractorModule {
	@Provides
	public ISubtractor providesSubtractor(Subtractor subtractor) {
		return subtractor;
	}
}

package com.jdw.calculator;

import javax.inject.Inject;

import com.jdw.calculator.api.IAdder;
import com.jdw.calculator.api.ICalculator;
import com.jdw.calculator.api.IMultiplier;
import com.jdw.calculator.api.ISubtractor;

/**
 * The Class Calculator.
 */
public class Calculator
        implements ICalculator
{
    private IAdder adder;
    private ISubtractor subtractor;
    private IMultiplier multiplier; 

    /**
     * Instantiates a new calculator.
     *
     * @param adder
     *            the adder
     * @param subtractor
     *            the subtractor
     * @param multiplier
     *            the multiplier
     */
    @Inject
    public Calculator(IAdder adder,
                      ISubtractor subtractor,
                      IMultiplier multiplier)
    {
        this.adder = adder;
        this.subtractor = subtractor;
        this.multiplier = multiplier;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jdw.calculator.api.ICalculator#add(int, int)
     */
    @Override
    public int add(int x,
                   int y)
    {
        return adder.add(x,
                         y);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jdw.calculator.api.ICalculator#subtract(int, int)
     */
    @Override
    public int subtract(int x,
                        int y)
    {
        return subtractor.subtract(x,
                                             y);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jdw.calculator.api.ICalculator#multiply(int, int)
     */
    @Override
    public int multiply(int x,
                        int y)
    {
        return multiplier.multiply(x,
                                   y);
    }
}
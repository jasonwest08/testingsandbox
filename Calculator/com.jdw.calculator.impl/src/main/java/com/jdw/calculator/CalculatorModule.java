package com.jdw.calculator;

import com.jdw.calculator.api.ICalculator;

import dagger.Module;
import dagger.Provides;

@Module
public class CalculatorModule {
	@Provides
	public  ICalculator providesCalculator(Calculator calculator) {
		return calculator;
	}
}

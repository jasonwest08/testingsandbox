package com.jdw.calculator;

import java.util.Arrays;

import javax.inject.Inject;

import com.jdw.calculator.api.IAdder;

public class Adder implements IAdder
{
	@Inject
	public Adder ()
	{
		
	}
	
	@Override
	public int add(int... values)
	{
		return Arrays.stream(values).sum();
	}
}

package com.jdw.calculator;

import com.jdw.calculator.api.IMultiplier;

import dagger.Module;
import dagger.Provides;

@Module
public class MultiplierModule {
	@Provides
	public 	IMultiplier providesMultiplier(Multiplier multiplier) {
		return multiplier;
	}
}

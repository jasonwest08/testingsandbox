package com.jdw.calculator;

import javax.inject.Inject;

import com.jdw.calculator.api.IMultiplier;

public class Multiplier implements IMultiplier
{
	@Inject
	public Multiplier ()
	{
		
	}
	
	@Override
	public int multiply(int x, int y)
	{
	    return x * y;
	}
}

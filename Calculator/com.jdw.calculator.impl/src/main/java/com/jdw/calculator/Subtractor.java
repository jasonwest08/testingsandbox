package com.jdw.calculator;

import javax.inject.Inject;

import com.jdw.calculator.api.ISubtractor;

public class Subtractor implements ISubtractor
{
	@Inject
	public Subtractor() {
		
	}
	
	@Override
	public int subtract(Integer a, Integer b)
	{
		return a - b;
	}
}

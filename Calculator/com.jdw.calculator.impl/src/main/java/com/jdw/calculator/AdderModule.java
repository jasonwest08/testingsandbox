package com.jdw.calculator;

import com.jdw.calculator.api.IAdder;

import dagger.Module;
import dagger.Provides;

@Module
public class AdderModule {
	@Provides
	public  IAdder providesAdder(Adder adder) {
		return adder;
	}
}

package com.jdw.calculator.ui;

import java.awt.event.ActionListener;

import javax.inject.Inject;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Bindings;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class CalculatorAppView {
	private final CalculatorAppVm vm;
	private final JFrame window = new JFrame("Calculator");
	private final JPanel mainPanel = new JPanel();
	private JButton button0;
	private JTextField display;

	@Inject
	public CalculatorAppView(CalculatorAppVm vm) {
		this.vm = vm;

		window.setName("calculatorAppFrame");
		initLayout();
	}

	public void show() {
		window.setVisible(true);
	}

	private void initLayout() {
		window.getContentPane().add(mainPanel);

		mainPanel.setLayout(new FormLayout(new ColumnSpec[] { FormSpecs.UNRELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormSpecs.UNRELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
				FormSpecs.UNRELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormSpecs.UNRELATED_GAP_COLSPEC, },
				new RowSpec[] { FormSpecs.UNRELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
						FormSpecs.UNRELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
						FormSpecs.UNRELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
						FormSpecs.UNRELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
						FormSpecs.UNRELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
						FormSpecs.UNRELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
						FormSpecs.UNRELATED_GAP_ROWSPEC, }));

		JButton buttonEquals = new JButton("=");
		JButton buttonMinus = new JButton("-");
		JButton buttonPlus = new JButton("+");
		button0 = new JButton("0");
		JButton button1 = new JButton("1");
		JButton button2 = new JButton("2");
		JButton button3 = new JButton("3");
		JButton button4 = new JButton("4");
		JButton button5 = new JButton("5");
		JButton button6 = new JButton("6");
		JButton button7 = new JButton("7");
		JButton button8 = new JButton("8");
		JButton button9 = new JButton("9");
		display = new JTextField();

		display.setName("display");
		button0.setName("zeroButton");

		ActionListener numButtonAction = (e) -> vm.enterNumber(((JButton) e.getSource()).getText());

		ActionListener operatorButtonAction = (e) -> {
			try {
				vm.enterOperator(((JButton) e.getSource()).getText());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		};

		ActionListener equalButtonAction = (e) -> {
			try {
				vm.enterEquals();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		};

		button0.addActionListener(numButtonAction);
		button1.addActionListener(numButtonAction);
		button2.addActionListener(numButtonAction);
		button3.addActionListener(numButtonAction);
		button4.addActionListener(numButtonAction);
		button5.addActionListener(numButtonAction);
		button6.addActionListener(numButtonAction);
		button7.addActionListener(numButtonAction);
		button8.addActionListener(numButtonAction);
		button9.addActionListener(numButtonAction);

		buttonPlus.addActionListener(operatorButtonAction);
		buttonMinus.addActionListener(operatorButtonAction);

		buttonEquals.addActionListener(equalButtonAction);

		// @formatter:off
		mainPanel.add(buttonMinus, "2, 12,       fill, fill");
		mainPanel.add(buttonPlus, "4, 12,       fill, fill");
		mainPanel.add(buttonEquals, "6, 12,       fill, fill");
		mainPanel.add(button0, "4, 10,       fill, fill");
		mainPanel.add(button1, "2,  8,       fill, fill");
		mainPanel.add(button2, "4,  8,       fill, fill");
		mainPanel.add(button3, "6,  8,       fill, fill");
		mainPanel.add(button4, "2,  6,       fill, fill");
		mainPanel.add(button5, "4,  6,       fill, fill");
		mainPanel.add(button6, "6,  6,       fill, fill");
		mainPanel.add(button7, "2,  4,       fill, fill");
		mainPanel.add(button8, "4,  4,       fill, fill");
		mainPanel.add(button9, "6,  4,       fill, fill");
		mainPanel.add(display, "2,  2, 5, 1, fill, fill");
		// @formatter:on

		window.pack();
		// window.setSize(400, 600);
		window.setLocationRelativeTo(null);

		initBindings();
	}

	private void initBindings() {
		// @formatter:off
		// PROPERTIES
		BeanProperty<CalculatorAppVm, String> displayTextProperty = BeanProperty
				.create(CalculatorAppVm.PROP_DISPLAY_TEXT);
		// ELProperty<CalculatorAppVm, String> displayTextProperty =
		// ELProperty.create("${" + CalculatorAppVm.PROP_DISPLAY_TEXT + " }");
		BeanProperty<JTextField, String> textFieldTextProperty = BeanProperty.create("text");

		// BINDINGS
		AutoBinding<CalculatorAppVm, String, JTextField, String> displayTextBinding = Bindings
				.createAutoBinding(UpdateStrategy.READ_WRITE, vm, displayTextProperty, display, textFieldTextProperty);

		// BINDS
		displayTextBinding.bind();

		// @formatter:on
	}

	// Just for test demo
	public void doSomethingOnAGuiObject() {
		button0.doClick();
	}
}

package com.jdw.calculator.ui;

import javax.inject.Inject;

import com.jdw.calculator.api.ICalculator;

// TODO All of this logic belongs in a model, not in the bloody vm
public class CalculatorAppVm
        extends AbstractBaseViewModel
{
    public static final String PROP_DISPLAY_TEXT = "displayText";

    // Collaborators
    private ICalculator calculator;

    // Fields
    private String queuedOperator = "";
    private Integer storedValue = null;
    private String displayText = "";

    private boolean queuedOperatorJustEntered = false;

	private boolean newCalculation = true;

	@Inject
    public CalculatorAppVm(ICalculator calculator)
    {
        this.calculator = calculator;
    }

    public void enterNumber(String text)
    {
        String newText = "";
        
        if (newCalculation)
        {
        	newCalculation = false;
        	storedValue = null;
        	newText += text;	
        }
        else if (queuedOperatorJustEntered)
        {
        	queuedOperatorJustEntered = false;
        	newText += text;
        }
        else
        {
        	newText = displayText + text;
        }

        setDisplayText(newText);
    }

    public void enterEquals()
            throws Exception
    {
        performQueuedOperation();
        newCalculation  = true;
    }

    public void enterOperator(String operator)
            throws Exception
    {
        // first char is operator... ignore it
        if (storedValue == null && displayText.isEmpty())
        {
            return;
        }

        // If we just entered an operator, lets just change the existing one
        if (queuedOperatorJustEntered)
        {
            queuedOperator = operator;
            return;
        }
        // Otherwise lets perform the queued op and then set the new operator settings
        else
        {
            performQueuedOperation();
            queuedOperator = operator;
            queuedOperatorJustEntered = true;
        }
    }

    private void performQueuedOperation()
            throws Exception
    {
        Integer displayValue = Integer.parseInt(displayText);

        // If we dont already have a value to operate on, just keep showing the
        // current display value
        if (storedValue == null)
        {
            storedValue = displayValue;
        }
        else
        {
            // TODO use the calculator here
            switch (queuedOperator)
            {
            case "+":
                storedValue = calculator.add(storedValue, displayValue);
                //storedValue += displayValue;
                break;
            case "-":
                //storedValue = calculator.subtract(storedValue, displayValue);
                break;
            default:
                throw new Exception("Unknown operator: " + queuedOperator);
            }
        }

        setDisplayText(storedValue.toString());
    }

    public String getDisplayText()
    {
        return displayText;
    }

    public void setDisplayText(final String newValue)
    {
        String oldValue = this.displayText;
        this.displayText = newValue;
        this.pcs.firePropertyChange(PROP_DISPLAY_TEXT,
                                    oldValue,
                                    newValue);

    }
}

package com.jdw.calculator.ui;

import java.beans.PropertyChangeListener;

import javax.swing.event.SwingPropertyChangeSupport;

public class AbstractBaseViewModel
{
    protected final SwingPropertyChangeSupport pcs = new SwingPropertyChangeSupport(this, true);

    public void addPropertyChangeListener(final PropertyChangeListener listener)
    {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(final String propertyName,
                                          final PropertyChangeListener listener)
    {
        pcs.addPropertyChangeListener(propertyName,
                                      listener);
    }

    public void removePropertyChangeListener(final PropertyChangeListener listener)
    {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(final String propertyName,
                                             final PropertyChangeListener listener)
    {
        pcs.removePropertyChangeListener(propertyName,
                                         listener);
    }

    protected void firePropertyChange(final String propertyName,
                                      final Object oldValue,
                                      final Object newValue)
    {
        pcs.firePropertyChange(propertyName,
                               oldValue,
                               newValue);
    }
}
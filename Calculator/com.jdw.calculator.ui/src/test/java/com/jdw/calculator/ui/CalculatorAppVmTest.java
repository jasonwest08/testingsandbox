//package com.jdw.calculator.ui;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.verifyNoMoreInteractions;
//import static org.mockito.Mockito.when;
//import static org.mockito.ArgumentMatchers.*;
//
//import org.assertj.core.api.SoftAssertions;
//import org.mockito.ArgumentCaptor;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//
//import com.jdw.calculator.ICalculator;
//
//public class CalculatorAppVmTest
//{
//    private ICalculator mockCalculator;
//    private CalculatorAppVm vm_CUT;
//
//    @BeforeMethod
//    public void setUp()
//            throws Exception
//    {
//        mockCalculator = mock(ICalculator.class);
//
//        vm_CUT = new CalculatorAppVm(mockCalculator);
//    }
//
//    @Test
//    public void testCalculatorAppVm()
//            throws Exception
//    {
//        CalculatorAppVm vm = new CalculatorAppVm(mockCalculator);
//        assertThat(vm).isNotNull();
//    }
//
//    @Test
//    public void testTwoCalculationsCanBePerformed()
//            throws Exception
//    {
//        // Test with a mock implementation for calculator.add
//        when(mockCalculator.add(1,
//                                2)).thenReturn(3);
//        vm_CUT.enterNumber("1");
//        vm_CUT.enterOperator("+");
//        vm_CUT.enterNumber("2");
//        vm_CUT.enterEquals();        
//        final int sum12  = Integer.valueOf(vm_CUT.getDisplayText()); 
//        assertThat(sum12).isEqualTo(3);
//
//        when(mockCalculator.add(3,
//                                4)).thenReturn(7);
//        vm_CUT.enterNumber("3");
//        vm_CUT.enterOperator("+");
//        vm_CUT.enterNumber("4");
//        vm_CUT.enterEquals();        
//        final int sum34  = Integer.valueOf(vm_CUT.getDisplayText());
//        assertThat(sum34).isEqualTo(7);
//
//        // Verify the dependency's add method was only called twice
//        // Capture the argument of the calculator's add function
//        ArgumentCaptor<Integer> captor1 = ArgumentCaptor.forClass(Integer.class);
//        ArgumentCaptor<Integer> captor2 = ArgumentCaptor.forClass(Integer.class);
//        verify(mockCalculator,
//               times(2)).add(captor1.capture(),
//                             captor2.capture());
//        verifyNoMoreInteractions(mockCalculator); 
//
//        int actualArg1_1 = captor1.getAllValues().get(0);
//        int actualArg2_1 = captor2.getAllValues().get(0);
//        int actualArg1_2 = captor1.getAllValues().get(1);
//        int actualArg2_2 = captor2.getAllValues().get(1);
//
//        // use SoftAssertions instead of direct assertThat methods
//        SoftAssertions softly = new SoftAssertions();
//
//        // Verify the dependency's add method was only called once
//        softly.assertThat(actualArg1_1).as("First argument").isEqualTo(1);
//        softly.assertThat(actualArg2_1).as("Second argument").isEqualTo(2);
//        softly.assertThat(actualArg1_2).as("First argument").isEqualTo(3);
//        softly.assertThat(actualArg2_2).as("Second argument").isEqualTo(4);
//
//        // Don't forget to call SoftAssertions global verification !
//        softly.assertAll();
//    }
//
//    @Test
//    public void testCalculatorOperatorMethodsAreCalledAppropriateNumberOfTimes()
//            throws Exception
//    {
//        when(mockCalculator.add(anyInt(), anyInt())).thenReturn(1);
//
//        verify(mockCalculator, times(0)).add(anyInt(), anyInt());
//        vm_CUT.enterNumber("1");
//        vm_CUT.enterOperator("+");
//        vm_CUT.enterNumber("2");
//        vm_CUT.enterEquals();        
//
//        verify(mockCalculator, times(1)).add(anyInt(), anyInt());
//        vm_CUT.enterNumber("3");
//        vm_CUT.enterOperator("+");
//        vm_CUT.enterNumber("4");
//        vm_CUT.enterEquals();        
//
//        // Verify the dependency's add method was only called twice, and nothing else was called
//        verify(mockCalculator, times(2)).add(anyInt(), anyInt());
//        verifyNoMoreInteractions(mockCalculator); 
//    }
//
//    @Test
//    public void testCalculatorOperatorMethodsAreCalledWithCorrectArguments()
//            throws Exception
//    {
//        vm_CUT.enterNumber("1");
//        vm_CUT.enterOperator("+");
//        vm_CUT.enterNumber("2");
//        vm_CUT.enterEquals();        
//
//        vm_CUT.enterNumber("3");
//        vm_CUT.enterOperator("+");
//        vm_CUT.enterNumber("4");
//        vm_CUT.enterEquals();     
//
//        // Verify the dependency's add method was only called twice
//        // Capture the argument of the calculator's add function
//        ArgumentCaptor<Integer> captor1 = ArgumentCaptor.forClass(Integer.class);
//        ArgumentCaptor<Integer> captor2 = ArgumentCaptor.forClass(Integer.class);
//        verify(mockCalculator,
//               times(2)).add(captor1.capture(),
//                             captor2.capture());
//        verifyNoMoreInteractions(mockCalculator); 
//
//        int actualArg1_1 = captor1.getAllValues().get(0);
//        int actualArg2_1 = captor2.getAllValues().get(0);
//        int actualArg1_2 = captor1.getAllValues().get(1);
//        int actualArg2_2 = captor2.getAllValues().get(1);
//
//        // use SoftAssertions instead of direct assertThat methods
//        SoftAssertions softly = new SoftAssertions();
//
//        // Verify the dependency's add method was only called once
//        softly.assertThat(actualArg1_1).as("First argument").isEqualTo(1);
//        softly.assertThat(actualArg2_1).as("Second argument").isEqualTo(2);
//        softly.assertThat(actualArg1_2).as("First argument").isEqualTo(3);
//        softly.assertThat(actualArg2_2).as("Second argument").isEqualTo(4);
//
//        // Don't forget to call SoftAssertions global verification !
//        softly.assertAll();
//    }
//}

//package com.jdw.calculator.ui;
//
//import static org.assertj.core.api.Assertions.assertThatThrownBy;
//import static org.assertj.swing.core.matcher.JButtonMatcher.withName;
//import static org.assertj.swing.core.matcher.JButtonMatcher.withText;
//import static org.assertj.swing.finder.WindowFinder.findFrame;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.verifyNoMoreInteractions;
//
//import org.assertj.core.api.SoftAssertions;
//import org.assertj.swing.annotation.GUITest;
//import org.assertj.swing.core.EmergencyAbortListener;
//import org.assertj.swing.edt.GuiActionRunner;
//import org.assertj.swing.exception.EdtViolationException;
//import org.assertj.swing.fixture.FrameFixture;
//import org.assertj.swing.fixture.JButtonFixture;
//import org.assertj.swing.fixture.JTextComponentFixture;
//import org.assertj.swing.testng.listener.ScreenshotOnFailureListener;
//import org.assertj.swing.testng.testcase.AssertJSwingTestngTestCase;
//import org.mockito.ArgumentCaptor;
//import org.testng.annotations.AfterMethod;
//import org.testng.annotations.Listeners;
//import org.testng.annotations.Test;
//
//import com.jdw.calculator.ICalculator;
//
////******************************************************************************
//// IMPORTANT NOTE: Mouse positions will be off if the display scaling feature
////                 in Windows is anything other than 100%.
////******************************************************************************
//
//@GUITest
//@Listeners(
//{
//  ScreenshotOnFailureListener.class
//})
//public class CalculatorAppIT
//        extends AssertJSwingTestngTestCase
//{
//    private EmergencyAbortListener listener;
//
//    private FrameFixture window;
//    private JButtonFixture equals;
//    private JButtonFixture plus;
//    private JButtonFixture minus;
//    private JButtonFixture zero;
//    private JButtonFixture one;
//    private JButtonFixture two;
//    private JButtonFixture three;
//    private JButtonFixture four;
//    private JButtonFixture five;
//    private JButtonFixture six;
//    private JButtonFixture seven;
//    private JButtonFixture eight;
//    private JButtonFixture nine;
//    private JTextComponentFixture display;
//    private CalculatorAppView view;
//
//    private ICalculator mockCalculator;
//    
//    // TODO notify infinitest of the bug when there is a failure in setup. Create SCCCE
//    @Override
//    public void onSetUp()
//    {
//        robot().settings().delayBetweenEvents(100);
//        robot().settings().clickOnDisabledComponentsAllowed(false);
//        
//        mockCalculator = mock(ICalculator.class);
//                
//        view = GuiActionRunner.execute(() -> new CalculatorAppView(new CalculatorAppVm(mockCalculator)));
//        GuiActionRunner.execute(() -> view.show());
//        window = findFrame("calculatorAppFrame").using(robot());
//
//        // CTRL+SHIFT+A will interrupt any running tests - the interrupt key combo can be configured
//        listener = EmergencyAbortListener.registerInToolkit(); // .keyCombination(key(VK_C).modifiers(SHIFT_MASK));
//
//        // TODO move these into a method that is called from the tests that need them
//        plus = window.button(withText("+"));
//        minus = window.button(withText("-"));
//        equals = window.button(withText("="));
//        
//        zero = window.button(withName("zeroButton").andText("0"));
//        one = window.button(withText("1"));
//        two = window.button(withText("2"));
//        three = window.button(withText("3"));
//        four = window.button(withText("4"));
//        five = window.button(withText("5"));
//        six = window.button(withText("6"));
//        seven = window.button(withText("7"));
//        eight = window.button(withText("8"));
//        nine = window.button(withText("9"));
//        
//        display = window.textBox("display");
//    }
//
//    @AfterMethod
//    public void afterEachTest()
//    {
//        listener.unregister();
//
//        // clean up resources.
//    }
//
//    @Test
//    public void testShow()
//    {
//        window.requireVisible();
//    }
//
//    @Test
//    public void testEdtViolation()
//    {
//        assertThatThrownBy(() ->
//        {
//            view.doSomethingOnAGuiObject();
//        }).isInstanceOf(EdtViolationException.class);
//    }  
//
//    @Test
//    public void testClickButtons_ShouldShowValuesOnDisplay()
//    {
//        zero.click();
//        one.click();
//        two.click();
//        three.click();
//        four.click();
//        five.click();
//        six.click();
//        seven.click();
//        eight.click();
//        nine.click();
//        
//        display.requireText("0123456789");
//    }    
//
//    @Test
//    public void testEnteringOnlyOperators_ShouldNotChangeDisplay()
//            throws Exception
//    {
//        plus.click();
//        display.requireText("");
//        
//        minus.click();
//        display.requireText("");
//        
//        plus.click();
//        display.requireText("");
//        
//        minus.click();
//        display.requireText("");
//        
//        plus.click();        
//        display.requireText("");
//    }
//
//    
//    @Test
//    public void testEnteringNumbersThenPressingOperator_ShouldStillShowNumberOnDisplay()
//            throws Exception
//    {
//        one.click();
//        zero.click();
//        
//        plus.click();
//        
//        display.requireText("10");
//    }
//
//    @Test
//    public void testEnteringNumbersAfterPressingOperator_ShouldShowOnlyNewNumberOnDisplay()
//            throws Exception
//    {
//        one.click();
//        zero.click();
//
//        plus.click();
//        
//        one.click();
//        zero.click();
//        zero.click();
//        
//        display.requireText("100");
//    }
//    
//    @Test
//    public void testPressingSecondOperator_ShouldShowResult()
//            throws Exception
//    {
//        one.click();
//        
//        plus.click();
//        
//        two.click();
//
//        plus.click();
//
//
//        // Capture the argument of the subtract function
//        ArgumentCaptor<Integer> captor1 = ArgumentCaptor.forClass(Integer.class);
//        ArgumentCaptor<Integer> captor2 = ArgumentCaptor.forClass(Integer.class);
//        verify(mockCalculator, times(1)).add(captor1.capture(), captor2.capture());
//        verifyNoMoreInteractions(mockCalculator); 
//
//        // Assert the argument
//        int actual1 = captor1.getValue();
//        int actual2 = captor2.getValue();
//
//        // use SoftAssertions instead of direct assertThat methods
//        SoftAssertions softly = new SoftAssertions();
//        
//        softly.assertThat(actual1).as("First argument").isEqualTo(1);
//        softly.assertThat(actual2).as("Second argument").isEqualTo(2);
//
//        // Don't forget to call SoftAssertions global verification !
//        softly.assertAll();
//    }
//    
//    @Test
//    public void testPressingEqual_AfterNumOperatorNum_ShouldShowResult()
//            throws Exception
//    {
//        one.click();
//        
//        plus.click();
//        
//        two.click();
//
//        equals.click();
//
//        // Capture the argument of the subtract function
//        ArgumentCaptor<Integer> captor1 = ArgumentCaptor.forClass(Integer.class);
//        ArgumentCaptor<Integer> captor2 = ArgumentCaptor.forClass(Integer.class);
//        verify(mockCalculator, times(1)).add(captor1.capture(), captor2.capture());
//        verifyNoMoreInteractions(mockCalculator); 
//
//        // Assert the argument
//        int actual1 = captor1.getValue();
//        int actual2 = captor2.getValue();
//
//        // use SoftAssertions instead of direct assertThat methods
//        SoftAssertions softly = new SoftAssertions();
//        
//        softly.assertThat(actual1).as("First argument").isEqualTo(1);
//        softly.assertThat(actual2).as("Second argument").isEqualTo(2);
//
//        // Don't forget to call SoftAssertions global verification !
//        softly.assertAll();
//    }
//}

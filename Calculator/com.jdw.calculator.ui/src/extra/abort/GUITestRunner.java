/*
 * Created on Mar 13, 2009
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * Copyright @2009 the original author or authors.
 */
package org.fest.swing.junit.v4_5.runner;

import org.assertj.swing.core.*;
import org.assertj.swing.junit.runner.FailureScreenshotTaker;
import org.assertj.swing.junit.runner.ImageFolderCreator;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.*;

import java.lang.annotation.*;
import java.util.Arrays;

/**
 * Understands a JUnit 4.5 test runner that takes a screenshot of a failed GUI test and
 * can provide a mechanism to abort running tests via a keyboard key press.
 *
 * @author Alex Ruiz
 * @author Yvonne Wang
 */
public class GUITestRunner extends BlockJUnit4ClassRunner implements AbortListener {

  /**
   * A static flag for all instances in the JVM that indicates if the user has requested
   * the tests to be aborted during execution.
   */
  private static boolean _userAborted;

  private final FailureScreenshotTaker screenshotTaker;

  @Retention(RetentionPolicy.RUNTIME)
  @Target(ElementType.TYPE)
  @Inherited
  public @interface AbortKeyPress {

    /** @return the classes to be run */
    public int[] value();
  }

  /**
   * Obtains the key press combination to invoke an abort when running the tests.
   *
   * @param klass The class that the tests are being created from.
   * @return The key press information to trigger an abort, or <code>null</code> if no
   *         key press information has been provided.
   * @throws InitializationError If there is an error with the specified key press info.
   */
  private static KeyPressInfo getAbortKeyPress(Class<?> klass)
          throws InitializationError {
    AbortKeyPress annotation = klass.getAnnotation(AbortKeyPress.class);
    if (annotation != null) {
      int[] keyValues = annotation.value();
      if (keyValues == null || keyValues.length < 1) {
        throw new InitializationError("Key Press values must be specified as ONE key " +
                                      "(e.g. KeyEvent.VK_A) and zero or more modifiers " +
                                      "(e.g. Event.CTRL_MASK)");
      }

      int keyCode = keyValues[0];
      KeyPressInfo abortKeyPress = KeyPressInfo.keyCode(keyCode);

      if (keyValues.length > 1) {
        int[] modifiers = Arrays.copyOfRange(keyValues, 1, keyValues.length);
        abortKeyPress.modifiers(modifiers);
      }
      return abortKeyPress;
    }
    return null;
  }

  /**
   * Creates a new <code>{@link GUITestRunner}</code>.
   * @param testClass the class containing the tests to run.
   * @throws InitializationError if something goes wrong when creating this runner.
   */
  public GUITestRunner(Class<?> testClass) throws InitializationError {
    super(testClass);

    KeyPressInfo abortKeyPress = getAbortKeyPress(testClass);

    EmergencyAbortListener abortListener = EmergencyAbortListener.registerInToolkitWithNoListeners()
            .withAbortListener(this)
            .withAbortListener(new BasicTestTerminator());
    if (abortKeyPress != null) {
      abortListener.keyCombination(abortKeyPress);
    }

    screenshotTaker = new FailureScreenshotTaker(new ImageFolderCreator().createImageFolder());
  }

  /**
   * Returns a <code>{@link Statement}</code> that invokes {@code method} on {@code test}. The created statement will
   * take and save the screenshot of the desktop in case of a failure.
   */
  @Override   protected Statement methodInvoker(FrameworkMethod method, Object test) {
    return new MethodInvoker(method, test, screenshotTaker);
  }

  public void abortTriggered() {
    _userAborted = true;
  }

  @Override
  protected void runChild(FrameworkMethod method, RunNotifier notifier) {
    checkForAbort();
    super.runChild(method, notifier);
  }

  @Override
  public void run(RunNotifier notifier) {
    checkForAbort();
    super.run(notifier);
  }

  protected void checkForAbort() {
    if (_userAborted) {
      throw new UserInvokedAbortException();
    }
  }
}
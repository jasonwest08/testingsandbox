package org.assertj.swing.core;

/**
 * An exception for when the User invokes an abort from the running tests. This exception
 * exists so that it can be specifically caught, if necessary, and to provide a more
 * obvious indication that tests were aborted upon a User request when stack traces are
 * generated from aborting tests.
 */
public class UserInvokedAbortException extends RuntimeException {

  public UserInvokedAbortException() {
    super("Tests have been aborted upon User request");
  }
}
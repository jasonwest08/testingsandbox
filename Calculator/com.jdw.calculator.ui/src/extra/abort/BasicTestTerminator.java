package org.assertj.swing.core;

import static org.assertj.swing.util.AWTExceptionHandlerInstaller.installAWTExceptionHandler;

/**
 * A rudimentary test terminator that provides clean up of any existing frames and
 * terminates the currently running test (but not its enclosing suite).
 */
public class BasicTestTerminator implements AbortListener {

  protected final FrameDisposer frameDisposer;

  public BasicTestTerminator() {
    this(new FrameDisposer());
  }

  public BasicTestTerminator(FrameDisposer frameDisposer) {
    this.frameDisposer = frameDisposer;
  }

  public void abortTriggered() {
    terminateTests();
  }

  void terminateTests() {
    frameDisposer.disposeFrames();
    throw new UserInvokedAbortException();
  }

  static {
    // Make sure there's an exception handler that will dump a stack trace on abort.
    installAWTExceptionHandler(SimpleFallbackExceptionHandler.class);
  }
}
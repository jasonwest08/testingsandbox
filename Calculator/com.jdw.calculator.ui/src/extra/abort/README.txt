I have attached the modified source files that can provide a solution to this problem when running under JUnit 4.5+. 
It was necessary to use a static "user aborted" flag in the GUITestRunner to ensure that the tests when running as 
a suite (or via the maven-surefire-plugin) to all be aborted. The use of the static flag allows for tests to be 
terminated provided that they share the same JVM. This means that for the maven-surefire-plugin, fork modes of 
never and once are supported but, a fork mode of always is not as a new JVM is created for each test. I cannot 
see that being much of a problem for GUI tests in general, but if it is then, some kind of "abort file" on disk
would be a possible solution as this would exist outside of the JVM. I played with that idea at first, but it 
was more hassel than it was worth as I was not interested in the always fork mode when running tests and it 
became difficult to "clean up" after an abort was triggered.

The attached files preserve the current behaviour added with the EmergencyAbortListener, but also add in the 
necessary hooks to modify the abort behaviour so that suites can be aborted when using a GUITestRunner. If you 
were to break backwards compatibility, then the supplied code could be tidied up a little more.

The GUITestRunner also has an annotation @GuiTestRunner.AbortKeyPress that allows for developers to specify a 
custom key press to abort the running tests, which will most likely be necessary as the default CTRL+SHIFT+A 
is a common shortcut for select all in our code.
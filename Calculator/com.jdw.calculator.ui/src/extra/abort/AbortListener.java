package org.assertj.swing.core;

/**
 * A Listener for User abort requests when testing.
 *
 * @author Peter Murray
 */
public interface AbortListener {

  /**
   * Triggered when a User invokes an abort on the currently running tests.
   * <p/>
   * Typically this will be triggered from the {@link EmergencyAbortListener} via a User
   * key press.
   */
  void abortTriggered();
}
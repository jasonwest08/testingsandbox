//package com.jdw.calculator.ui.test;
//
//import cucumber.api.java.After;
//import cucumber.api.java.Before;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class StepDefs
//{
//    CalculatorSupport calculatorSupport;
//    
//    @Before
//    public void before()
//    {
//        calculatorSupport = new CalculatorSupport();
//    }
//    
//    @After
//    public void after()
//    {
//        calculatorSupport.cleanUp();
//    }
//    
//    @Given("^I just opened the calculator$")
//    public void i_just_opened_the_calculator()
//            throws Exception
//    {
//        calculatorSupport.launchApp();
//    }
//
//    @When("^I enter the number (\\d+)$")
//    public void i_enter_the_number(int value)
//            throws Exception
//    {
//        calculatorSupport.enterValue(value);
//    }
//
//    @When("^I press \"(.+)\"$")
//    public void i_press(String buttonText)
//            throws Exception
//    {
//        calculatorSupport.press(buttonText);
//    }
//
//    @Then("^I should see (\\d+) displayed$")
//    public void i_should_see_displayed(int displayValue)
//            throws Exception
//    {
//        calculatorSupport.verifyDisplay(displayValue);
//    }
//}
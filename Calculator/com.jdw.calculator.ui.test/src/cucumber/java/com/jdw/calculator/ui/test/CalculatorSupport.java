//package com.jdw.calculator.ui.test;
//
//import static org.assertj.swing.core.matcher.JButtonMatcher.withText;
//import static org.assertj.swing.core.matcher.JTextComponentMatcher.withName;
//import static org.assertj.swing.finder.WindowFinder.findFrame;
//
//import org.assertj.swing.core.BasicRobot;
//import org.assertj.swing.core.Robot;
//import org.assertj.swing.edt.GuiActionRunner;
//import org.assertj.swing.fixture.FrameFixture;
//
//import com.jdw.calculator.Adder;
//import com.jdw.calculator.Calculator;
//import com.jdw.calculator.IAdder;
//import com.jdw.calculator.ICalculator;
//import com.jdw.calculator.ui.CalculatorAppView;
//import com.jdw.calculator.ui.CalculatorAppVm;
//
//public class CalculatorSupport
//{
//    private final ICalculator calculator;
//    private Robot robot;
//    private FrameFixture frameFixture;
//
//    public CalculatorSupport()
//    {
//        final IAdder adder = new Adder();
//        //final ISubtractor subtractor = new Subtractor();
//        //final IMultiplier multiplier = new Multiplier();
//
//        calculator = new Calculator(adder);//, subtractor, multiplier);
//        robot = BasicRobot.robotWithNewAwtHierarchy();
//
//        robot.settings().delayBetweenEvents(0);
//        robot.settings().clickOnDisabledComponentsAllowed(false);
//        
//    }
//
//    public void launchApp()
//    {
//        frameFixture = getFrameFixture();
//    }
//
//    private FrameFixture getFrameFixture()
//    {
//        CalculatorAppView view = GuiActionRunner.execute(() -> new CalculatorAppView(new CalculatorAppVm(calculator)));
//        GuiActionRunner.execute(() -> view.show());
//
//        return findFrame("calculatorAppFrame").using(robot);
//    }
//
//    public void cleanUp()
//    {
//        robot.cleanUp();
//        frameFixture.cleanUp();
//    }
//
//    public void enterValue(int value)
//    {
//        String.valueOf(value).chars().forEach(eachInt -> press(String.valueOf((char) eachInt)));
//    }
//    
//    public void press(String buttonText)
//    {
//        frameFixture.button(withText(buttonText)).click();
//    }
//
//    public void verifyDisplay(int displayValue)
//    {
//        frameFixture.textBox(withName("display")).requireText(String.valueOf(displayValue));        
//    }
//}

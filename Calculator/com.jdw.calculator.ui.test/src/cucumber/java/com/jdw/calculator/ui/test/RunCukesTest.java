package com.jdw.calculator.ui.test;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin =
{
  "pretty"
}, glue =
{
  "com.jdw.calculator.ui.test"
})
public class RunCukesTest
{
    
}
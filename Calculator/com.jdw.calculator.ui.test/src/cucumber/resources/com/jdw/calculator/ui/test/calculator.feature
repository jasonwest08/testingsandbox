#Sample Feature Definition Template
@arithmetic
Feature: Arithmetic
	I want to use the calculator for arithmetic

@arithmentic
Scenario Outline: Addition
  Given I just opened the calculator
  When I enter the number <value1>
   And I press "+"
   And I enter the number <value2>
   And I press "="
  Then I should see <sum> displayed

Examples:
    | value1 | value2 | sum |  
    |    1   |    2   |   3 |  
    |   17   |  100   | 117 |  

@arithmetic
Scenario Outline: Subtraction
  Given I just opened the calculator
  When I enter the number <value1>
   And I press "-"
   And I enter the number <value2>
   And I press "="
  Then I should see <difference> displayed

Examples:
    | value1 | value2 | difference |  
    |    2   |    1   |      1     |  
    |   10   |    6   |      4     |  

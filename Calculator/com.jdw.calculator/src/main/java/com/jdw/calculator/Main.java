package com.jdw.calculator;

import com.jdw.calculator.ui.CalculatorAppView;

import dagger.Component;

public class Main {
	@Component(modules = { CalculatorModule.class, AdderModule.class, SubtractorModule.class, MultiplierModule.class })
	public interface IMaker {
		CalculatorAppView make();
	}

	public static void main(String[] args) {
		IMaker maker = DaggerMain_IMaker.builder().build();

		final CalculatorAppView viewDagger = maker.make();
		viewDagger.show();
		
//		IAdder adder = new Adder();
//		ISubtractor subtractor = new Subtractor();
//		IMultiplier multiplier = new Multiplier();
//		ICalculator calculator = new Calculator(adder, subtractor, multiplier);
//		final CalculatorAppVm vm = new CalculatorAppVm(calculator);
//		final CalculatorAppView view = new CalculatorAppView(vm);
//		
//		view.show();
	}
}

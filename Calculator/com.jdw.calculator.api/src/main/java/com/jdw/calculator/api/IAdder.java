package com.jdw.calculator.api;

public interface IAdder
{
	int add(int... values);
}

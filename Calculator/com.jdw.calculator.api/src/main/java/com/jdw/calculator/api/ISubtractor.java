package com.jdw.calculator.api;

public interface ISubtractor
{
	int subtract(Integer a, Integer b);
}

package com.jdw.calculator.api;

public interface ICalculator
{
    /**
     * Adds two integers.
     *
     * @param x
     *            the x
     * @param y
     *            the y
     * @return the int result
     */
    int add(int x,
            int y);

    /**
     * Subtracts two integers.
     *
     * @param x
     *            the number to subtract from
     * @param y
     *            the number to subtract
     * @return the int result
     */
    int subtract(int x,
                 int y);

    /**
     * Multiplies two integers
     *
     * @param x
     *            the x
     * @param y
     *            the y
     * @return the product of x and y
     */
    int multiply(int x,
                 int y);
}

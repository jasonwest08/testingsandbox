package com.jdw.calculator.api;

public interface IMultiplier
{
    int multiply(int x,
                 int y);
}

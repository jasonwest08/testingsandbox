package com.jdw.cukes;

import static org.assertj.core.api.Assertions.assertThat;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs
{
    private Belly belly;

    @Before
    public void before()
    {
        belly = new Belly();
    }

    @After
    public void after()
    {

    }

    @Given("^I have (\\d+) (?:cukes?) in my belly$")
    public void I_have_cukes_in_my_belly(int cukes)
            throws Throwable
    {
        belly.eat(cukes);
    }

    @When("^I wait (\\d+) (?:hours?)$")
    public void i_wait_hour(int hours)
            throws Exception
    {
        belly.wait(hours);
    }

    @Then("^my belly should growl (\\d+) (?:times?)$")
    public void my_belly_should_growl(int growls)
            throws Exception
    {
        assertThat(belly.getGrowls()).isEqualTo(growls);
    }
}
package com.jdw.cukes;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@RunWith(Cucumber.class)
@CucumberOptions(
        // features =
        // {
        // "src/test/cukes/resources"
        // },
plugin =
{
  "pretty",
  "json:target/cucumber-report.json",
  "html:target/cucumber-report"
})
public class RunCukesTest
        extends AbstractTestNGCucumberTests
{
}
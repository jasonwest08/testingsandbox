Feature: Belly

Scenario: a few cukes
    Given I have 1 cuke in my belly
    When I wait 1 hour
    Then my belly should growl 2 times
    
Scenario Outline: Title of your scenario outline
		Given I have <cukes> cukes in my belly
		When I wait <hours> hours
		Then my belly should growl <growls> times

Examples:
    | cukes | hours | growls |
    | 12    | 2     | 4      |
    | 3     | 5     | 10     |
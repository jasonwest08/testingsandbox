package coffee;

import dagger.Lazy;

import javax.inject.Inject;

import coffee.heater.IHeater;
import coffee.pump.IPump;

class CoffeeMaker {
	private final Lazy<IHeater> heater; // Create a possibly costly heater only when we use it.
	private final IPump pump;

	@Inject
	CoffeeMaker(Lazy<IHeater> heater, IPump pump) {
		this.heater = heater;
		this.pump = pump;
	}

	public void brew() {
		heater.get().on();
		pump.pump();
		System.out.println(" [_]P coffee! [_]P ");
		heater.get().off();
	}
}

package coffee;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

import coffee.heater.ElectricHeater;
import coffee.heater.IHeater;
import coffee.pump.IPump;
//import coffee.pump.PumpModule;
import coffee.pump.Thermosiphon;

@Module//(includes = PumpModule.class)
public class DripCoffeeModule {
	@Provides
	@Singleton
	IHeater provideHeater() {
		return new ElectricHeater();
	}
	
	// Binding a concrete class to an interface for injection...
	// this is better handled with "Binds", but is harder to read,
	// so for now I won' do it
	@Provides
	IPump providePump(Thermosiphon pump) {
		return pump;
	}
}

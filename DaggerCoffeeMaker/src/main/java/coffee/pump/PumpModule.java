package coffee.pump;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class PumpModule {

	// Binding a concrete class to an interface for injection...
	@Binds
	abstract IPump providePump(Thermosiphon pump);
}

package coffee.pump;

import javax.inject.Inject;

import coffee.heater.IHeater;

public class Thermosiphon implements IPump {
	private final IHeater heater;

	@Inject
	public 	Thermosiphon(IHeater heater) {
		this.heater = heater;
	}

	@Override
	public void pump() {
		if (heater.isHot()) {
			System.out.println("=> => pumping => =>");
		}
	}
}

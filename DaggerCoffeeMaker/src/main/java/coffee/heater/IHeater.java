package coffee.heater;

public interface IHeater {
  void on();
  void off();
  boolean isHot();
}
